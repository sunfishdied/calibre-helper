/**
 * Created by rescue on 10/11/14.
 */
import com.sunfish.calibre_worker.worker;
import org.apache.log4j.BasicConfigurator;

import java.io.*;
import java.util.Hashtable;
import java.util.Properties;

public class cli {

    public static void main(String[] args) throws Exception {

        String strHelp="Sunfish's Calibre Helper Tool\n ";
        System.out.print(strHelp);
        File dir = new File("");
        String workingPath=dir.getAbsolutePath()+"/";
        BasicConfigurator.configure();

        worker w ;

        if(args.length==0) {
            System.out.print("Using predefined properties config\n");
            Properties p = new Properties();
            InputStreamReader isr = new InputStreamReader(new FileInputStream(workingPath + "calibreh.properties"), "UTF-8");
            p.load(isr);
            String scopes = p.getProperty("job");
            if (scopes.length() == 0) {
                System.out.print("no predefined jobs found , check config file plz");}
                String[] Scopes = scopes.split(",");

                for (String Scope : Scopes) {

                    String libpath = p.getProperty("lib." + Scope);
                    String workJob = p.getProperty("job." + Scope);
                    String sPattern = p.getProperty("pattern." + Scope);
                    Integer seriesWidth = Integer.parseInt(p.getProperty("series_width"));

                    String[] Jobs = workJob.split(",");


                    for(String Job:Jobs) {
                        switch (Job) {
                            case "t":
                                String sTags = p.getProperty("STAG." + Scope);
                                try {
                                    w = new worker(Job);
                                    w.SeriesWidth = seriesWidth;
                                    w.MakeTagHardLink(libpath, sPattern, sTags, Job);
                                    w.close();
                                } catch (Exception ee) {
                                    System.out.print(ee.getMessage());
                                }
                                break;
                            case "s":
                                try {

                                    w = new worker(Job);
                                    w.MakeTagHardLink(libpath, sPattern, "", Job);
                                    w.close();
                                } catch (Exception ee) {
                                    System.out.print(ee.getMessage());
                                }
                                break;
                            case "l":


                                break;
                            default:
                        }
                    }

                }


        }else {
            w=new worker("");
            w.CleanTagAndDuplicates(args[0]);

        }

    }



}
