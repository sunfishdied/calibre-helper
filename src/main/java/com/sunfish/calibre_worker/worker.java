package com.sunfish.calibre_worker;

/**
 * Created by sunfish on 10/11/14.
 */

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.*;
public class worker {

   // private String elementTag="(,[";

    public Integer SeriesWidth=2;

    private HashMap<String,String> elementTags = new HashMap<String, String>();


    //region SQL Related
    private String sqliteConnectionString = "jdbc:sqlite://%1$s";
    private String classForDriver="org.sqlite.JDBC";
    private String sqlGetTags="select id, name from tags";
    private String sqlGetMaxTagid="select max(id) from tags";
    private String sqlInsertOrIgnoreTags="insert or ignore into tags ( id, name) values (? , ?)";
    private String sqlGetBooksContainingElement = "select id, title from books where instr( title, '%1$s' ) >0 ";
    private String sqlUpdateBookTitle = "update books set title = ? where id = ? ";
    private String sqlAddTagLinkToBook = "insert or ignore into books_tags_link (book, tag ) values (?, ?) ";
    private String sqlGetIDbyTitle = "select id from books where title = ? ";
    private String tagName_Duplication="_DUP_";
    private String sqlGetDuplicatedTitles="select title from (select count(*) as cnt, title from books group by title) where cnt>1";
    private String sqlUnsetALLTag="delete from books_tags_link where tag= ? ";
    private String sqlSetAuthorSort="update books set author_sort =? where id= ?";
    private String sqlEmptyAuthorSort="update books set author_sort=''";
    private String sqlGetBookPathFromTag="select title,a.id,c.name,path||'/'||d.name||'.'||lower(d.format) as path,lower(d.format) as format from books a join (select min(author) as author,book from  books_authors_link group by book) b on (a.id=b.book) join authors c  on (b.author=c.id ) join data d  on (a.id=d.book) where a.id in ( select book from books_tags_link where tag in ( select id from tags where name= ? ))";
    private String sqlGetSeriesList="select name from series";
    private String sqlGetBookPathFromSeries="select title,a.id,c.name,path||'/'||d.name||'.'||lower(d.format) as path,\n" +
            "lower(d.format) as nfn,series_index\n" +
            "from\n" +
            " books a\n" +
            "join (select min(author) as author,book from  books_authors_link group by book) b\n" +
            "\ton (a.id=b.book)\n" +
            "join authors c \n" +
            "\ton (b.author=c.id )\n" +
            "join data d \n" +
            "\ton (a.id=d.book)\n" +
            "where a.id in (\n" +
            "select book from books_series_link where \n" +
            "series in (\n" +
            "select id from series where name= ? \n" +
            "));";
    private String sqlMarkMalformatedTitle="";

    private String sqlDropTrigger1="drop trigger book_insert_trg";
    private String sqlDropTrigger2="drop trigger books_update_trg";

    //endregion

    public Integer maxTagID=0;
    private Connection con;
    private static Logger log;
    public  HashMap<String,Integer> Tags;


    public worker(String wn)
    {
        log=Logger.getLogger(wn);
        elementTags.put("[","]");
        elementTags.put("(",")");
        elementTags.put("【","】");
        elementTags.put("（","）");
    }

    public void MakeTagHardLink (String fn,String pattern,String sTags,String format)
    {
        try {

            String targetPath = fn.substring(0,fn.lastIndexOf("/"))+"/";
            String targetRoot = targetPath + pattern.substring(0,pattern.indexOf("/"));

            //region check if hardlink target already exists


            File f = new File(targetRoot);

            if(f.exists()){
                File fbak=new File(targetRoot+".old");
                if(fbak.exists())
                {
                    FileUtils.deleteDirectory(fbak);
                }
                f.renameTo(new File(targetRoot+".old"));
                f.mkdir();
            }else
            {
                f.mkdir();
            }

            //endregion

            Class.forName(classForDriver);
            //String currentSQL = "";
            con = DriverManager.getConnection(String.format(sqliteConnectionString, fn+"/metadata.db"));
            //Statement st = con.createStatement();

            //region link book by series

            if(format.equals("s"))
            {
                String nfn;

                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(sqlGetSeriesList);
                PreparedStatement ps = con.prepareStatement(sqlGetBookPathFromSeries);
                while(rs.next())
                {
                    String series_name = rs.getString(1);
                    log.info("series: " + series_name);
                    String originalSeriesName=series_name;
                    series_name = strCleanse(series_name);

                        File fd = new File(targetRoot + "/" + series_name);
                        fd.mkdir();


                    ps.setString(1,originalSeriesName);
                    ResultSet psr = ps.executeQuery();
                    while(psr.next())
                    {
                        String title=psr.getString(1);
                        String id = psr.getString(2);
                        String author=psr.getString(3);
                        String path=psr.getString(4);
                        String ext="."+psr.getString(5);
                        String series_index = psr.getString(6);




                        if(series_index.endsWith(".0"))
                            series_index=series_index.substring(0,series_index.length()-2);

                        if(title.contains(series_name))
                            title=title.replace(series_name,"");


                        title = strCleanse(title);

                            if(title.contains(series_name)&& title.contains(series_index))
                        {
                            title="";
                        }

                        //series_width padding
                        while(series_index.length()<SeriesWidth)
                        {
                            series_index = "0"+series_index;
                        }

                        if(title.trim().equals(series_index)) title="";

                        if(series_name.contains("/"))
                            series_name=series_name.replace("/","_");

                        nfn=targetPath+pattern;

                        nfn = nfn.replace("[TITLE]",title)
                                 .replace("[AUTHOR]", author)
                                 .replace("[SERIES]", series_name)
                                 .replace("[SERIES_INDEX]", series_index)
                                + ext;
                        Boolean isOldExists=false;
                        try {
                            Path pnew=Paths.get(nfn);
                            Path pold=Paths.get(fn + "/" + path);
                            isOldExists = Files.exists(pold);
                            Files.createLink(pnew,pold);
                        }catch (IOException ioe)
                        {
                            if(isOldExists) log.warn("original file missing: "+fn + "/" + path); else
                            log.error(ioe.getMessage()+"\n"+id+":  "+targetPath+path+" => "+nfn);
                        }
                    }
                }
            }


            //endregion link book by series

            //region link books by tag
            if(format.equals("t"))
            {


                String[] STag = sTags.split(",");

                PreparedStatement ps = con.prepareStatement(sqlGetBookPathFromTag);
                String nfn;
                for (String sTag : STag)
                {
                    ps.setString(1, sTag);
                    ResultSet rs = ps.executeQuery();
                    File fd= new File(targetRoot+"/"+sTag);
                    fd.mkdir();
                    log.info(sTag);


                    while(rs.next())
                    {
                        String title=rs.getString(1);
                        String id = rs.getString(2);
                        String author=rs.getString(3);
                        String path=rs.getString(4);
                        String ext="."+rs.getString(5);
                        nfn=targetPath+pattern;
                        title = strCleanse(title);

                        nfn = nfn.replace("[TITLE]",title).replace("[STAG]",sTag).replace("[AUTHOR]",author) + ext;
                        try {
                            Path pnew=Paths.get(nfn);
                            Path pold=Paths.get(fn + "/" + path);
                            Boolean isOldExists = Files.exists(pold);
                            Files.createLink(pnew,pold);
                        }catch (IOException ioe)
                        {
                            log.error(ioe.getMessage()+"\n"+id+":  "+targetPath+path+" => "+nfn);
                        }
                    }
                }
            }
            //endregion link books by tag


        }catch (Exception ex)
        {
            log.info(ex.getMessage());
        }


    }

    public void  CleanTagAndDuplicates(String fn)
    {
        try {
            Class.forName(classForDriver);
            String currentSQL="";

            con = DriverManager.getConnection(String.format(sqliteConnectionString,fn));

            Statement st = con.createStatement();

            try
            {
                st.execute(sqlEmptyAuthorSort);
                st.execute(sqlDropTrigger1);

            }catch (Exception ex)
            {
                log.info(ex.getMessage());
            }
            try
            {
                st.execute(sqlDropTrigger2);

            }catch (Exception ex)
            {
                log.info(ex.getMessage());
            }



            log.warn("loading calibre's tags ");

            Tags = new HashMap<String, Integer>();
            ResultSet rsTags=  st.executeQuery(sqlGetTags);
            while(rsTags.next())
            {
                Tags.put(rsTags.getString(2),rsTags.getInt(1));
            }

            rsTags.close();

            rsTags =  st.executeQuery(sqlGetMaxTagid);
            while(rsTags.next())
            {
                maxTagID = rsTags.getInt(1);
            }
            rsTags.close();
            log.warn("examine book title ");
            PreparedStatement paddTag = con.prepareStatement(sqlInsertOrIgnoreTags);
            PreparedStatement passignTag =con.prepareStatement(sqlAddTagLinkToBook);
            PreparedStatement pupdateTitle = con.prepareStatement(sqlUpdateBookTitle);
            PreparedStatement assignDupSeq = con.prepareStatement(sqlSetAuthorSort);

            String ce="",cd="";

            //region Parse Quotations
            for(Map.Entry<String,String>  e : elementTags.entrySet()) {
                ce = e.getKey();
                cd = elementTags.get(ce);

                log.warn("searching for element " + ce);

                currentSQL = String.format(sqlGetBooksContainingElement, ce);
                ResultSet rs = st.executeQuery(currentSQL);
                String title = "", newTitle = "", ele = "";
                Integer bookID = 0;
                while (rs.next()) {
                    title = rs.getString(2).trim();
                    bookID = rs.getInt(1);
                    boolean toReplace = true;
                    while (toReplace) {
                        int start, end;
                        start = title.indexOf(ce);
                        end = title.indexOf(cd, start);
                        if (end > start & start >=0 ) {
                            newTitle = title.substring(start + 1, end);
                            ele = newTitle.toUpperCase();

                            if (Tags.containsKey(ele)) {
                                passignTag.setInt(1, bookID);
                                passignTag.setInt(2, Tags.get(ele));
                                passignTag.execute();
                                //log.warn(String.format("set tag %1$s : %2$s ", ele, bookID));
                            } else {

                                maxTagID++;
                                Tags.put(ele, maxTagID);
                                paddTag.setInt(1, maxTagID);
                                paddTag.setString(2, ele);
                                boolean k = paddTag.execute();



                                //log.warn(String.format("new  tag %1$s : %2$s ", ele, maxTagID));
                                passignTag.setInt(1, bookID);
                                passignTag.setInt(2, Tags.get(ele));
                                passignTag.execute();
                                //log.warn(String.format("set tag %1$s : %2$s ", ele, bookID));
                            }

                            title = title.replace(ce + newTitle + cd, "");

                        } else {
                            toReplace = false;
                            pupdateTitle.setString(1, title.trim());
                            pupdateTitle.setInt(2, bookID);
                            pupdateTitle.execute();



                        }
                    }

                    if( (title.contains(ce) && ! title.contains(cd)))
                    {

                        assignDupSeq.setInt(2,bookID);
                        assignDupSeq.setString(1,"_unbalaced_"+ce);
                        assignDupSeq.execute();
                        log.info("_unbalaced_"+ce +" @ "+bookID);
                    }

                    if(!(title.contains(ce) && title.contains(cd)))
                    {
                        assignDupSeq.setInt(2,bookID);
                        assignDupSeq.setString(1,"_unbalaced_"+cd);
                        assignDupSeq.execute();
                        log.info("_unbalaced_"+cd +" @ "+bookID);

                    }
                }
            }


            //endregion

            //region Duplication Detect
            if(!Tags.containsKey(tagName_Duplication))
            {
                maxTagID++;
                Tags.put(tagName_Duplication,maxTagID);
                paddTag.setInt(1, maxTagID);
                paddTag.setString(2, tagName_Duplication);
                paddTag.execute();

                log.warn(String.format("new  tag %1$s : %2$s ", tagName_Duplication, maxTagID));
            }
            Integer tagDup = Tags.get(tagName_Duplication);
            ResultSet rs =st.executeQuery(sqlGetDuplicatedTitles);
            PreparedStatement ps_CleanDupTag = con.prepareStatement(sqlUnsetALLTag);

            ps_CleanDupTag.setInt(1,tagDup);
            ps_CleanDupTag.execute();
            ps_CleanDupTag.close();
            log.warn("Duplication Tag Cleared ");

            PreparedStatement ps2 = con.prepareStatement(sqlGetIDbyTitle);
            Integer dupSerial=1;

            while(rs.next()) {
                String bookTitle = rs.getString(1);
                log.info("Duplicaton Detected @ "+bookTitle);
                ps2.setString(1, bookTitle);
                Integer bookID = 0;
                dupSerial++;
                ResultSet rs2 = ps2.executeQuery();
                while (rs2.next()) {
                    bookID = rs2.getInt(1);
                    passignTag.setInt(1, bookID);
                    passignTag.setInt(2, tagDup);
                    passignTag.execute();

                    assignDupSeq.setInt(2,bookID);
                    assignDupSeq.setString(1,dupSerial.toString());
                    assignDupSeq.execute();


                    log.info("Duplication Tag "+ tagName_Duplication+dupSerial.toString()+"< set on id "+bookID);
                }
            }

            //endregion Duplication Detect

            //region mal-formated title detection

            //endregion

        }
        catch (ClassNotFoundException classNF)
        {
            log.error("sqlite-jdbc not found @"+classNF.getMessage());
        }
        catch (SQLException se)
        {
            log.error("sqlite error @"+se.getMessage());
        }

    }

    public static String strCleanse(String inStr)
    {
        String s = inStr;
        HashMap<String,String> reserves=  new HashMap<String,String>();
        reserves.put("<","【");
        reserves.put(">","】");
        reserves.put("\"","“");
        reserves.put("/","_");
        reserves.put("\\","_");
        reserves.put("|","_");
        reserves.put("?","？");
        reserves.put("*","_");
        reserves.put(":","：");



        for(Map.Entry<String,String> m : reserves.entrySet())
        {
            if(inStr.contains(m.getKey()))
            {

               s = s.replace(m.getKey(),m.getValue());
            }
        }
        return s;

    }
    public void close(){
        try{
        con.close();}catch (Exception ee){};
        log=null;

    }
}
